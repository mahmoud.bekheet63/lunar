import {Component, OnInit} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from "chart.js";
import {Color, Label, SingleDataSet} from "ng2-charts";

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss']
})

export class DashboardPageComponent implements OnInit {
  lineChartData: ChartDataSets[] = [
    {
      pointRadius: 0,
      pointHoverRadius: 0,
      borderWidth: 3,
      data: [300, 310, 316, 322, 330, 326, 333, 345, 338, 354],
      label: 'Series A'
    },
    {
      pointRadius: 0,
      pointHoverRadius: 0,
      borderWidth: 3,
      data: [320, 340, 365, 360, 370, 385, 390, 384, 408, 420],
      label: 'Series B'
    },
    {
      pointRadius: 0,
      pointHoverRadius: 0,
      borderWidth: 3,
      data: [370, 394, 415, 409, 425, 445, 460, 450, 478, 484],
      label: 'Series C'
    },
  ];
  lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  lineChartOptions: ChartOptions = {
    responsive: true,
  };
  lineChartColors: Color[] = [
    {
      borderColor: "#6bd098",
      backgroundColor: "#6bd098",
    },
    {
      borderColor: "#f17e5d",
      backgroundColor: "#f17e5d",
    },
    {
      borderColor: "#fcc468",
      backgroundColor: "#fcc468",
    },
  ];
  lineChartLegend = true;
  lineChartType: ChartType = 'line';
  lineChartPlugins = [];


  lineDotsChartData: ChartDataSets[] = [
    {
      data: [0, 19, 15, 20, 30, 40, 40, 50, 25, 30, 50, 70],
      fill: false,
      pointRadius: 4,
      pointHoverRadius: 4,
      pointBorderWidth: 8,
      label: 'Series A'
    },
    {
      data: [0, 5, 10, 12, 20, 27, 30, 34, 42, 45, 55, 63],
      fill: false,
      pointRadius: 4,
      pointHoverRadius: 4,
      pointBorderWidth: 8,
      label: 'Series B'
    },
  ];
  lineDotsChartLabels: Label[] = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  lineDotsChartOptions: ChartOptions = {
    responsive: true,
  };
  lineDotsChartColors: Color[] = [
    {
      borderColor: '#fbc658',
      pointBorderColor: '#fbc658',
      backgroundColor: 'transparent',
    },
    {
      borderColor: '#51CACF',
      backgroundColor: 'transparent',
      pointBorderColor: '#51CACF',
    }
  ];
  lineDotsChartLegend = false;
  lineDotsChartType: ChartType = 'line';
  lineDotsChartPlugins = [];


  pieChartOptions: ChartOptions = {
    responsive: true,
  };
  pieChartLabels: Label[] = [
    'Unopened',
    'Opened',
    'Read',
    'Deleted'
  ];
  pieChartData: SingleDataSet = [342, 480, 530, 120];
  pieChartType: ChartType = 'pie';
  pieChartLegend = false;
  pieChartPlugins = [];
  pieChartColors: Color[] = [
    {
      backgroundColor: [
        '#e3e3e3',
        '#4acccd',
        '#fcc468',
        '#ef8157'
      ],
    },

  ];

  ngOnInit(): void {

  }

}
