import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  config: any;
  fullPage_api: any;

  constructor() {
    this.config = {
      navigation: true,
      scrollingSpeed: 500,
      responsiveHeight: 0,
      paddingTop: '4.2em',
      easing: 'easeInOutCubic',
      easingcss3: 'ease',
    };
  }

  ngOnInit(): void {
  }

  getRef(fullPageRef: any) {
    this.fullPage_api = fullPageRef;
  }

}
