import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomePageComponent} from "./home-page/home-page.component";
import {NotFoundComponent} from "./not-found/not-found.component";
import {PricingPageComponent} from "./pricing-page/pricing-page.component";
import {DashboardPageComponent} from "./dashboard-page/dashboard-page.component";

const routes: Routes = [
  {path: 'home', component: HomePageComponent},
  {path: 'pricing', component: PricingPageComponent},
  {path: 'dashboard', component: DashboardPageComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
